#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace std;
int n=4;
char arr[5][5] = {{'.X..'},{'....'},{'XX..'},{'....'} };
bool safe(int x,int y)
{
    for(int i=x; i>-1 && arr[i][y] != 'X'; i--) //checking upper part
        if(arr[i][y] == 'R') return 0;
    for(int i=y; i>-1 && arr[x][i] != 'X'; i--) //checking left part

        if(arr[x][i] == 'R') return 0;
        return 1;
}

int solve(int x,int y)
{
    if(y == n) return solve(x+1,0);
    if(x == n) return 0;

    int ret = 0;
    if(arr[x][y] == '.' && safe(x,y))
    {
        arr[x][y] = 'R';
        ret = 1 + solve(x,y+1);
        arr[x][y] = '.';
    }
    ret = max(ret, solve(x,y+1));
    return ret;
}

int main()
{
    int x=  solve(0,0) ;

    cout<< x ;
    return 0;
}
